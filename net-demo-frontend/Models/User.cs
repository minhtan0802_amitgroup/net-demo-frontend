using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
public class User
{
    public String email { get; set; }
    public String name { get; set; }
    public String password { get; set; }
    public String address { get; set; }
    public DateTime? birthday { get; set; }
    public int role_id { get; set; }
    public int gender { get; set; }
    public String status { get; set; }
    public DateTime? created_date { get; set; }
    public DateTime? updated_date { get; set; }
    public String? created_by { get; set; }
    public String? updated_by { get; set; }
    public String? description { get; set; }

}
public class UserAddModel{
    public String email { get; set; }
    public String name { get; set; }
    public String address{get;set;}
    public DateTime birthday{get;set;}
    public int gender{get;set;}
    public String? description{get;set;}
    public String created_by{get;set;}
    public int role_id{get;set;}
}