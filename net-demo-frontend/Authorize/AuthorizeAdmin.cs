
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

public class AuthorizeAdmin : ActionFilterAttribute
{
    public override void OnActionExecuted(ActionExecutedContext filterContext)
    {
        if ((net_demo_frontend.Controllers.LoginController.getAdmin()) == null)
        {
            var result = new ViewResult();
            result.ViewName = "~/Views/Login/Error.cshtml";
            filterContext.Result = result;
        }
    }
}