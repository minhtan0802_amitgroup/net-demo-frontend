using System.Net;
using Newtonsoft.Json;

public class ConnectAPI{
        public static object get(string path)
        {
            WebClient webClient = new WebClient()
            {
                Encoding = System.Text.Encoding.UTF8
            };
            using (webClient)
            {
                return JsonConvert.DeserializeObject<object>(webClient.DownloadString(path));
            }
        } 
      public static string url= "https://localhost:7118/";
       /* public static string url = "http://nvnptit.tk/api/";*/
}