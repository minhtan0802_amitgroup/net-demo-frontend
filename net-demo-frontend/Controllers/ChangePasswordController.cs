using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace net_demo_frontend.Controllers;
[Route("change-pass")]
public class ChangePasswordController : Controller
{
    [Route("")]
    public IActionResult Index()
    {
        return View();
    }
    [HttpPost]
    [Route("validate")]
    public ActionResult validate(ChangePassword changePass)
    {
        if(String.IsNullOrEmpty(changePass.oldPass)||String.IsNullOrEmpty(changePass.newPass)){
            return Json(new{success=false,message="Vui lòng điền đầy đủ thông tin!"});
        }
        HttpClientHandler clientHandler = new HttpClientHandler();
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        using (var client = new HttpClient(clientHandler))
        {
            try
            {
                var admin=net_demo_frontend.Controllers.LoginController.getAdmin();
                var user=net_demo_frontend.Controllers.LoginController.getUser();
                var email=admin!=null?admin.email:user.email;
               
                client.BaseAddress = new Uri(ConnectAPI.url);
                var response = client.PostAsJsonAsync("user/change-pass?email=" + email + "&oldPass="+changePass.oldPass+"&newPass="+changePass.newPass+"&updated_by="+"adm", "").Result;
                var a = response.Content.ReadAsStringAsync();
                JObject jobject = JObject.Parse(a.Result);
                var success = jobject["success"];
                JToken data;
                JToken message;
                if (!bool.Parse(success.ToString()))
                {
                    message = jobject["message"];
                    return Json(new { success = false, message = message.ToString() });
                }
                else
                {
                    data = jobject["data"];
                    Object u = new User();
                    u = data.ToObject(typeof(User));
                    return Json(new { success = true, data = u });
                }
            }
            catch (Exception e)
            {
                Console.Write("Lỗi: " + e.Message);
                return Json(new { success = false, message = "Lỗi " + e.Message });
            }
        }
    }
}