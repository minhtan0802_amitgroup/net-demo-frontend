using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace net_demo_frontend.Controllers;
[Route("register")]
public class RegisterController : Controller
{
    [HttpGet]
    [Route("")]
    public IActionResult Index()
    {
        return View();
    }
    [Route("validate")]
    [HttpPost]
    public JsonResult Ajax_Register(UserAddModel model)
    {
        if (String.IsNullOrEmpty(model.email) || String.IsNullOrEmpty(model.name))
        {
            return Json(new { success = false, message = "Email/ tên không được để trống!" });
        }
        HttpClientHandler clientHandler = new HttpClientHandler();
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        using (var client = new HttpClient(clientHandler))
        {
            try
            {
                client.BaseAddress = new Uri(ConnectAPI.url);
                var response = client.PostAsJsonAsync("user", model).Result;
                var a = response.Content.ReadAsStringAsync();
                JObject jobject = JObject.Parse(a.Result);
                var success = jobject["success"];
                JToken data;
                JToken message;
                if (!bool.Parse(success.ToString()))
                {
                    message = jobject["message"];
                    return Json(new { success = false, message = message.ToString() });
                }
                else
                {
                    data = jobject["data"];
                    User u = new User();
                    u = (User)data.ToObject(typeof(User));
                    net_demo_frontend.Controllers.LoginController.admin = u;
                    return Json(new { success = true, data = u });
                }
            }
            catch (Exception e)
            {
                Console.Write("Lỗi: " + e.Message);
                return Json(new { success = false, message = "Lỗi " + e.Message });
            }
        }

    }
}