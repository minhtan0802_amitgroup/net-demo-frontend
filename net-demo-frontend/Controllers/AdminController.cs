using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

[AuthorizeAdmin]
[Route("admin")]
public class AdminController : Controller
{

    [Route("")]
  
    public IActionResult Index()
    {
        return View();
    }
    [Route("logout")]
    public IActionResult Logout()
    {
        net_demo_frontend.Controllers.LoginController.admin = null;
        return View("~/Views/Login/Index.cshtml");
    }
    [HttpGet]
    [Route("category")]
    public IActionResult category()
    {
        return View("Category");
    }
    [HttpGet]
    [Route("list-category")]
    public JsonResult Ajax_Category()
    {
        HttpClientHandler clientHandler = new HttpClientHandler();
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        using (var client = new HttpClient(clientHandler))
        {
            try
            {
                List<Category> listCategory = new List<Category>();
                client.BaseAddress = new Uri(ConnectAPI.url);
                var response = client.GetAsync("category").Result;
                var a = response.Content.ReadAsStringAsync();
                JObject jobject = JObject.Parse(a.Result);
                var success = jobject["success"];
                JToken data;
                JToken message;
                List<Object> list;
                if (!bool.Parse(success.ToString()))
                {
                    message = jobject["message"];
                    return Json(new { success = false, message = message });
                }
                else
                {
                    data = jobject["data"];
                    Object p = new Category();
                    foreach (var item in data)
                    {
                        p = item.ToObject(typeof(Category));
                        listCategory.Add((Category)p);
                    }
                    return Json(new { success = true, data = JsonConvert.SerializeObject(listCategory)});
                }
            }
            catch (Exception e)
            {
                Console.Write("Lỗi: " + e.Message);
                return Json(new { success = false, message = "Lỗi " + e.Message });
            }
        }

    }
}