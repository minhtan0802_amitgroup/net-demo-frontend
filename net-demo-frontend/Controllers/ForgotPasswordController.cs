using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
namespace net_demo_frontend.Controllers;
[Route("forgot-pass")]
public class ForgotPasswordController : Controller
{

    [Route("")]
    public IActionResult Index()
    {
        return View();
    }
    [HttpPost]
    [Route("validate")]
    public ActionResult validate(Login login)
    {
        HttpClientHandler clientHandler = new HttpClientHandler();
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        using (var client = new HttpClient(clientHandler))
        {
            try
            {
                client.BaseAddress = new Uri(ConnectAPI.url);
                var response = client.PostAsJsonAsync("user/forgot-pass?email=" + login.email + "&updated_by=adminstrator", "").Result;
                var a = response.Content.ReadAsStringAsync();
                JObject jobject = JObject.Parse(a.Result);
                var success = jobject["success"];
                JToken data;
                JToken message;
                if (!bool.Parse(success.ToString()))
                {
                    message = jobject["message"];
                    return Json(new { success = false, message = message });
                }
                else
                {
                    data = jobject["data"];
                    Object u = new User();
                    u = data.ToObject(typeof(User));
                    return Json(new { success = true, data = u });
                }
            }
            catch (Exception e)
            {
                Console.Write("Lỗi: " + e.Message);
                return Json(new { success = false, message = "Lỗi " + e.Message });
            }
        }
    }
}