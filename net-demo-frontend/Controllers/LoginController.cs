using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
namespace net_demo_frontend.Controllers;
[Route("login")]
public class LoginController : Controller
{
    public static User user;
    public static User admin;
    public static String error = "";

    [Route("")]
    public IActionResult Index()
    {
        var error=HttpContext.Response.Headers["error"];
        if(error=="no"){
            ViewBag.error="Mời bạn vui lòng đăng nhập!";
        }
        else{
            ViewBag.error="";
        }
        return View();
    }
    [Route("validate")]
    [HttpPost]
    public JsonResult Ajax_validate(Login model)
    {
        if(String.IsNullOrEmpty(model.email)||String.IsNullOrEmpty(model.password)){
             return Json(new{success=false,message="Không được để trống email/ mật khẩu!"});
        }
        HttpClientHandler clientHandler = new HttpClientHandler();
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
        using (var client = new HttpClient(clientHandler))
        {
            try
            {
                client.BaseAddress = new Uri(ConnectAPI.url);
                var response = client.PostAsJsonAsync("user/login?emailInput=" + model.email + "&passwordInput=" +model.password, "").Result;
                var a = response.Content.ReadAsStringAsync();
                JObject jobject = JObject.Parse(a.Result);
                var success = jobject["success"];
                JToken data;
                JToken message;
                if (!bool.Parse(success.ToString()))
                {
                   message=jobject["message"];
                   return Json(new{success=false,message=message.ToString()});
                }
                else
                {
                    data = jobject["data"];
                    User u= new User();
                    u=(User)data.ToObject(typeof(User));
                    if(u.role_id==0){
                        user=u;
                    }
                    else
                    {
                        admin=u;
                    }
                    return Json(new{success=true,data=u});
                }
            }
            catch (Exception e)
            {
                Console.Write("Lỗi: " + e.Message);
                return Json(new { success = false, message = "Lỗi " + e.Message });
            }
        }
    }
    public static User getUser(){
        return user;
    }
    public static User getAdmin(){
        return admin;
    }
}