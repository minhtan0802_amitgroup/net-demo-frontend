using System.Collections.Specialized;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

public class UserDAO
{
    public static String login(string path, string email, string password)
    {
        // string inputJson = JsonSerializer.Serialize("");
        // WebClient client = new WebClient();
        // client.Headers["Content-type"] = "application/json";
        // client.Encoding = Encoding.UTF8;
        // string json = client.UploadString(path, "");
        // return json;
        using (var client = new WebClient())
        {
            var values = new NameValueCollection();
            values["email"] = email;
            values["password"] = password;

            var response = client.UploadValues(path, values);

            var responseString = Encoding.Default.GetString(response);
            return responseString;
        }
    }
}